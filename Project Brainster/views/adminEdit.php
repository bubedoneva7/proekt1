<?php
    
    include_once 'head.php';    
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Brainster.xyz Labs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- bootstrap CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- AOS animations http://michalsnik.github.io/aos/ -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
   
    <!-- custom CSS stylesheet -->
    <link rel="stylesheet" type="text/css" media="screen" href="views/main.css" />

    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />

    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

</head>
<body>
<!-- if($num_row > 0) {
    while($row = $projects->fetch()) {
        echo "<p>".$row['project_name']."</p>";
    }
}
else {
    echo "0 results";
} -->






<form action="../index.php" method="POST">
    
    image <input type="url" name="project_image" ><br>

    project name<input type="text" name="project_name"><br>

    project subname<input type="text" name="project_subname"><br>

    project content<input type="text" name="project_content"><br>
    <button type="submit">Submit</button>
</form>












        <?php include_once 'footer.php'; ?>
        
      
   
        <!-- bootstrap - js, jquery  -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
        <!-- AOS animation -->
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <!-- initialize AOS -->
        <script>
            AOS.init();
        </script>
    </body>
    </html>
