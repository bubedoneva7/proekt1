<?php

class Connection {
    
    public static function connect() {
        try {
            return new PDO('mysql:host=localhost;dbname=brainster_project_1;charset=utf8', 'root', '');
        }
        catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}

// $servername = 'localhost';
// $username = 'root';
// $password = '';
// $dbname = 'brainster_project_1';
