<!DOCTYPE html>
<html>

<body>
    <footer class="customFooter container mt-5 pt-3">
        <div class="row">
            <div class="col-4 text-right pt-3">
                <p class="text-secondary">Made with <i class="fas fa-heart text-danger"></i> by</p>
            </div>

            <div class="col-4 text-center">
                <img width="170" src="images/logo.png" alt="brainster logo">
            </div>

            <div class="col-4 text-left pt-3">
                    <a href="" class="text-secondary"><span class="text-success">Say Hi!</span>-Terms</a>
            </div>
        </div>
    </footer>

</body>
</html>

   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- AOS animation -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- initialize AOS -->
    <script>
        AOS.init();
    </script>
</body>
</html>