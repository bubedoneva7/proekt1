<?php
    
    include_once 'head.php';
    
    $requir = $_SERVER['REQUEST_URI'];

    if($requir == 'Project%20Brainster/views/admin.php') {
        header('Location: views/admin.php');
        die();
    }
    
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Brainster.xyz Labs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- bootstrap CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- AOS animations http://michalsnik.github.io/aos/ -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
   
    <!-- custom CSS stylesheet -->
    <link rel="stylesheet" type="text/css" media="screen" href="views/main.css" />

    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />

    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

</head>
<body>
        <div class="customBackground customTextColor">
            <h1 class="display-4 text-center" data-aos="fade-up"
            data-aos-anchor-placement="top-center">Brainster.xyz Labs</h1>
            <p class="lead text-center" data-aos="fade-up"
            data-aos-anchor-placement="top-center">Проекти на студентите на академиите за програмирање и маркетинг на Brainster</p>
        </div>

        

        <!-- <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 mt-5" data-aos="fade-up" data-aos-anchor-placement="center-center" data-aos-duration="500">
                    <div class="card">
                        <img class="card-img-top" src="https://www.designfreelogoonline.com/wp-content/uploads/2016/03/00106-3D-company-logo-design-free-logo-online-Template-03.png" alt="Card image cap">
                        <div class="car-body text-center px-3">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 mt-5" data-aos="fade-up" data-aos-anchor-placement="center-center" data-aos-duration="1000">
                    <div class="card">
                        <img class="card-img-top" src="https://www.designfreelogoonline.com/wp-content/uploads/2016/03/00106-3D-company-logo-design-free-logo-online-Template-03.png" alt="Card image cap">
                        <div class="car-body text-center px-3">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 mt-5" data-aos="fade-up" data-aos-anchor-placement="center-center" data-aos-duration="1500">
                    <div class="card">
                        <img class="card-img-top" src="https://www.designfreelogoonline.com/wp-content/uploads/2016/03/00106-3D-company-logo-design-free-logo-online-Template-03.png" alt="Card image cap">
                        <div class="car-body text-center px-3">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        </div>
                    </div>
                </div>
            </div>

             <div class="row">
                <div class="col-md-4 col-sm-12 mt-5" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-duration="500">
                    <div class="card">
                        <img class="card-img-top" src="https://www.designfreelogoonline.com/wp-content/uploads/2016/03/00106-3D-company-logo-design-free-logo-online-Template-03.png" alt="Card image cap">
                        <div class="car-body text-center px-3">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 mt-5" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-duration="1000">
                    <div class="card">
                        <img class="card-img-top" src="https://www.designfreelogoonline.com/wp-content/uploads/2016/03/00106-3D-company-logo-design-free-logo-online-Template-03.png" alt="Card image cap">
                        <div class="car-body text-center px-3">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 mt-5" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-duration="1500">
                    <div class="card">
                        <img class="card-img-top" src="https://www.designfreelogoonline.com/wp-content/uploads/2016/03/00106-3D-company-logo-design-free-logo-online-Template-03.png" alt="Card image cap">
                        <div class="car-body text-center px-3">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="py-5">
            <!-- pagination  -->
        </div>

        <?php include_once 'footer.php'; ?>
        
      
   
    <!-- bootstrap - js, jquery  -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- AOS animation -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- initialize AOS -->
    <script>
        AOS.init();
    </script>
</body>
</html>