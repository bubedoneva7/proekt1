<?php
require 'database/connection.php';

class QueryBuilder {
    public $project_image;
    public $project_name;
    public $project_subname;
    public $project_content;
 

    public function __construct($project_image, $project_name, $project_subname, $project_content) {

        $this->project_image = $project_image;
        $this->project_name = $project_name;
        $this->project_subname = $project_subname;
        $this->project_content = $project_content;
    }

    public function insert() {
        $pdo = Connection::connect();       

        $sql = "INSERT INTO projects (project_image, project_name, project_subname, project_content) VALUES(?,?,?,?)";

        $statment = $pdo->prepare($sql);

        
            if($statment->execute([$this->project_image, $this->project_name,       $this->project_subname, $this->project_content])) {
                
                header('Location:views/adminEdit.php');
                die();
                
            }
            else {
                echo "Something went wrong";
            }        
    }


    public function display() {
        $pdo = Connection::connect();

        $sql = "SELECT project_image, project_name, project_subname, project_content FROM projects";

        $statment = $pdo->query($sql);

        $statment->execute();

        return $statment;
    }
    
}


